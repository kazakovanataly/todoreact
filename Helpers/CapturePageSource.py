# -*- coding: utf-8 -*-
from robot.libraries.BuiltIn import BuiltIn


def _seleniumlib():
    return BuiltIn().get_library_instance(u"Selenium2Library")


class CapturePageSource(object):
    @staticmethod
    def capture_page_source():
        print(u"Page source:")
        print(_seleniumlib().get_source())

    @staticmethod
    def capture_log(type_log=u'browser'):
        """
        Usage:
                driver.get_log('browser')
                driver.get_log('driver')
                driver.get_log('server')
        :param type_log:
        :return: capture log
        """
        logs = BuiltIn().get_library_instance("Selenium2Library").driver.get_log(type_log)
        for entry in logs:
            print(entry)

    def capture_log_browser(self):
        self.capture_log(u'browser')

    def capture_log_driver(self):
        self.capture_log(u'driver')

    def capture_log_server(self):
        self.capture_log(u'server')
