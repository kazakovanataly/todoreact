# -*- coding: utf-8 -*-
*** Settings ***
Library  String
Library  PageObjects.ToDoReactPageObject

*** Keywords ***
Page Is Open
     Check Input

Add ToDo
     ${ToDo1}  Input And Save Name
     [Return]  ${ToDo1}

Check Add ToDo
    [Arguments]  ${ToDo1}
     Check Create Entry  ${ToDo1}

Delete ToDo
     Click Delete

Check Delete ToDo
    Check Delete Entry

Edit ToDo
   ${ToDo1_edit}  Edit Entry
    [Return]  ${ToDo1_edit}

Check Edit ToDo
    [Arguments]  ${ToDo1_edit}
    Check Create Entry  ${ToDo1_edit}

Complete ToDo
    Complete Entry

Check Complete ToDo
    Check Complete Entry

Add Second ToDo
     ${ToDo2}  Input And Save Name
     [Return]  ${ToDo2}

Check Add Second ToDo
     [Arguments]  ${ToDo2}
     Check Create Entry  ${ToDo2}

Filter Completed ToDo
    [Arguments]  ${ToDo1}  ${ToDo2}
    Check Filter Completed  ${ToDo1}  ${ToDo2}

Clear Completed
    Click Clear Completed

Check Clear Completed ToDo
    Check Clear Completed Entry

Check Number Completed
    ${Number}  Check Number Of Completed
    [Return]  ${Number}

Check Counter
    [Arguments]  ${Number}
    Check Counter On Footer  ${Number}

Delete All
    Filter All
    Delete All Entries

Check Delete All
    Check Delete Entry
